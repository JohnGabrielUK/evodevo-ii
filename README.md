# Evodevo II

## What?

An entry to the GMTK Game Jam 2021.

## Who?

Me, JohnGabrielUK. I reused quite a few sound effects from Evodevo, and copy-pasted some code from my old projects, but apart from that, it's all new. The music was by Kevin MacLeod, and, of course, the game was made with the Godot Engine.

## Can I?

Probably - the game is licensed under GNU GPL v3. The art and music are [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
