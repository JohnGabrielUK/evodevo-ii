extends RigidBody2D

class_name Asteroid

const _AsteroidDust : PackedScene = preload("res://objects/particles/AsteroidDust.tscn")
const _AsteroidDestroyed : PackedScene = preload("res://objects/AsteroidDestroyed.tscn")

onready var audio_hit : AudioStreamPlayer2D = $Audio_Hit

var health : int = 3

signal destroyed

func destroy() -> void:
	var destroyed : Node2D = _AsteroidDestroyed.instance()
	get_tree().current_scene.add_child(destroyed)
	destroyed.global_position = global_position
	for i in range(0, 3):
		var asteroid_dust : Sprite = _AsteroidDust.instance()
		get_tree().current_scene.add_child(asteroid_dust)
		asteroid_dust.global_position = global_position
		asteroid_dust.rotation = randf() * PI * 2.0
		asteroid_dust.scale = Vector2.ONE * (1.0 + (randf() * 0.5))
	get_tree().call_group("player_camera", "shake_camera", self.global_position, 0.5)
	emit_signal("destroyed")
	queue_free()

func get_hit() -> void:
	audio_hit.play()
	health -= 1
	if health <= 0:
		destroy()

func _physics_process(delta : float) -> void:
	if global_position.x < -16.0:
		queue_free()
