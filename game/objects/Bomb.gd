extends RigidBody2D

const _HugeExplosion : PackedScene = preload("res://objects/particles/HugeExplosion.tscn")

onready var sprite_lights : Sprite = $Sprite_Lights
onready var audio_tick : AudioStreamPlayer2D = $Audio_Tick
onready var tween : Tween = $Tween
onready var timer_tick : Timer = $Timer_Tick

var connected : bool = false
var time_left : int = 145

signal bomb_collected
signal bomb_detonated

func _on_ConnectorFemale_got_connection(owning_object : Node2D, snap_to : Position2D, linear_velocity : Vector2, angular_velocity : float) -> void:
	if connected:
		return
	connected = true
	emit_signal("bomb_collected")

func destroy() -> void:
	var explosion : Node2D = _HugeExplosion.instance()
	explosion.global_position = global_position
	get_tree().current_scene.call_deferred("add_child", explosion)
	queue_free()
	emit_signal("bomb_detonated")

func get_fatal_collision_speed() -> float:
	if GameSession.current_difficulty == GameSession.DifficultyMode.NORMAL:
		return 100.0
	else:
		return 1.0

func _on_EscapePod_body_entered(body) -> void:
	if body is Asteroid:
		var impact_velocity : float = (body.linear_velocity - self.linear_velocity).length()
		if impact_velocity > get_fatal_collision_speed():
			destroy()
	if body is TileMap:
		var impact_velocity : float = self.linear_velocity.length()
		if impact_velocity > get_fatal_collision_speed():
			destroy()

func _on_ConnectorFemale_attracted(diff : Vector2) -> void:
	if not connected:
		apply_impulse(Vector2.ZERO, diff * (1.0/5.0))

func _on_Timer_Tick_timeout() -> void:
	time_left -= 1
	if time_left < 0:
		destroy()
	else:
		audio_tick.pitch_scale = 1.0 if time_left > 30 else 2.0
		audio_tick.play()
		tween.interpolate_property(sprite_lights, "modulate", Color.white, Color.transparent, 0.2)
		tween.interpolate_property(sprite_lights, "modulate", Color.white, Color.transparent, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.3 if time_left > 30 else 0.2)
		tween.start()

func _ready() -> void:
	match GameSession.current_difficulty:
		GameSession.DifficultyMode.NORMAL:
			time_left = 175
		GameSession.DifficultyMode.EXTREME:
			time_left = 115
		_:
			time_left = 145
