extends RigidBody2D

const MOVE_SPEED : float = 40.0
const ROTATION_SPEED : float = 80.0
const MAX_SPEED : float = 16.0

export (NodePath) var path_target
onready var target = get_node(path_target)

onready var particles_thrust_forward_a : Particles2D = $Particles2D_Thrust_ForwardA
onready var particles_thrust_forward_b : Particles2D = $Particles2D_Thrust_ForwardB

func _physics_process(delta : float) -> void:
	particles_thrust_forward_a.emitting = false
	particles_thrust_forward_b.emitting = false
		
	if is_instance_valid(target):
		var target_angle : float = self.global_position.angle_to_point(target.global_position)
		var angle_diff : float = wrapf(target_angle - self.global_rotation, -PI, PI)
		if angle_diff > 0.0 and angular_velocity > -5.0:
			apply_torque_impulse(-ROTATION_SPEED * delta)
		elif angle_diff < 0.0 and angular_velocity < 5.0:
			apply_torque_impulse(ROTATION_SPEED * delta)
		
		var direction_to_target : Vector2 = self.global_position.direction_to(target.global_position)
		var velocity_to_target : float = (self.linear_velocity * direction_to_target).length()
		if true:
			apply_impulse(Vector2.ZERO, Vector2.RIGHT.rotated(rotation) * MOVE_SPEED * delta)
			particles_thrust_forward_a.emitting = true
			particles_thrust_forward_b.emitting = true
