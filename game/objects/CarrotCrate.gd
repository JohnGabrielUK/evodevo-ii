extends RigidBody2D

class_name CarrotCrate

const _Carrots : PackedScene = preload("res://objects/particles/Carrots.tscn")
const _Explosion : PackedScene = preload("res://objects/particles/SmallExplosion.tscn")

onready var audio_hit : AudioStreamPlayer2D = $Audio_Hit

var health : int = 3

var connected : bool = false

signal carrot_crate_destroyed

func _on_ConnectorFemale_got_connection(owning_object : Node2D, snap_to : Position2D, linear_velocity : Vector2, angular_velocity : float) -> void:
	if connected:
		return
	global_position = snap_to.global_position
	global_rotation = snap_to.global_rotation
	connected = true

func _on_ConnectorFemale_attracted(diff : Vector2) -> void:
	if not connected:
		apply_impulse(Vector2.ZERO, diff * (1.0/15.0))

func destroy() -> void:
	var explosion : Node2D = _Explosion.instance()
	explosion.global_position = global_position
	get_tree().current_scene.add_child(explosion)
	var carrots : Particles2D = _Carrots.instance()
	get_tree().current_scene.add_child(carrots)
	carrots.global_position = self.global_position
	carrots.emitting = true
	emit_signal("carrot_crate_destroyed")
	queue_free()

func get_hit() -> void:
	audio_hit.play()
	health -= 1
	if health <= 0:
		destroy()

func _physics_process(delta : float) -> void:
	if global_position.x < -16.0:
		queue_free()
