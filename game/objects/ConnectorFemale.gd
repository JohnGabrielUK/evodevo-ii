extends Area2D

class_name ConnectorFemale

export (NodePath) var path_belongs_to

onready var belongs_to = get_node(path_belongs_to)
onready var area_magnet : Area2D = $Area2D_Magnet

signal got_connection
signal attracted

func _physics_process(delta : float) -> void:
	for area in area_magnet.get_overlapping_areas():
		if area.is_in_group("connector_male"):
			var diff : Vector2 = area.global_position - self.global_position
			emit_signal("attracted", diff)

func get_connected(owning_object : Node2D, snap_to : Position2D, linear_velocity : Vector2, angular_velocity : float) -> void:
	emit_signal("got_connection", owning_object, snap_to, linear_velocity, angular_velocity)
