extends PinJoint2D

export (NodePath) var path_belongs_to

onready var collision_shape : CollisionShape2D = $Area2D/CollisionShape2D
onready var snap_to : Position2D = $Position2D_SnapTo
onready var belongs_to = get_node(path_belongs_to)
onready var audio_part_connected : AudioStreamPlayer2D = $Audio_PartConnected

var connected : bool = false

signal part_connected

func _area_entered(area : Area2D) -> void:
	if connected: return
	if area is ConnectorFemale:
		node_b = get_path_to(area.belongs_to)
		area.get_connected(belongs_to, snap_to, belongs_to.linear_velocity, belongs_to.angular_velocity)
		audio_part_connected.play()
		connected = true
		emit_signal("part_connected")
