extends Area2D

const _RockSpray : PackedScene = preload("res://objects/particles/RockSpray.tscn")
const _Sparks : PackedScene = preload("res://objects/particles/Sparks.tscn")

onready var trail : Sprite = $Trail

var velocity : Vector2

func _physics_process(delta : float) -> void:
	position += velocity * delta
	if trail.scale.y < 2.0:
		trail.scale.y += delta * 10.0

func _on_Bullet_body_entered(body) -> void:
	if body is Asteroid:
		body.get_hit()
		var spray : Particles2D = _RockSpray.instance()
		spray.global_position = global_position
		get_tree().current_scene.add_child(spray)
		spray.emitting = true
		queue_free()
	elif body is Player:
		body.get_hit()
		var sparks : Particles2D = _Sparks.instance()
		sparks.global_position = global_position
		get_tree().current_scene.add_child(sparks)
		sparks.emitting = true
		queue_free()

func _on_Timer_Despawn_timeout() -> void:
	queue_free()
