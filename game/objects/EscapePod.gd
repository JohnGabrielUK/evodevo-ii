extends RigidBody2D

const _Explosion : PackedScene = preload("res://objects/particles/Explosion.tscn")

onready var audio_hit : AudioStreamPlayer2D = $Audio_Hit

var connected : bool = false

signal pod_rescued
signal pod_destroyed
signal asteroid_hit # For achievements

func _on_ConnectorFemale_got_connection(owning_object : Node2D, snap_to : Position2D, linear_velocity : Vector2, angular_velocity : float) -> void:
	if connected:
		return
	connected = true
	emit_signal("pod_rescued")

func destroy() -> void:
	var explosion : Node2D = _Explosion.instance()
	explosion.global_position = global_position
	get_tree().current_scene.add_child(explosion)
	queue_free()
	emit_signal("pod_destroyed")

func _on_EscapePod_body_entered(body : PhysicsBody2D) -> void:
	if body is Asteroid:
		var impact_velocity : float = (body.linear_velocity - self.linear_velocity).length()
		if impact_velocity > 20.0:
			destroy()
		elif impact_velocity > 5.0:
			audio_hit.pitch_scale = 0.9 + (randf() * 0.2)
			audio_hit.volume_db = 0.0 if impact_velocity > 10.0 else -10.0 # janky hack, m8
			audio_hit.play()
		emit_signal("asteroid_hit")

func _on_ConnectorFemale_attracted(diff : Vector2) -> void:
	if not connected:
		apply_impulse(Vector2.ZERO, diff * (1.0/15.0))
