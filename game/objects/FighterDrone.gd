extends RigidBody2D

class_name FighterDrone

const _EnemyBullet : PackedScene = preload("res://objects/EnemyBullet.tscn")
const _Explosion : PackedScene = preload("res://objects/particles/Explosion.tscn")

const MOVE_SPEED : float = 64.0
const ROTATION_SPEED : float = 80.0
const MAX_SPEED : float = 32.0

const SHOT_SPEED : float = 100.0
const RECOIL_FORCE : float = 5.0

const CLIP_SIZE : int = 4

var target

onready var particles_thrust_forward_a : Particles2D = $Particles2D_Thrust_ForwardA
onready var particles_thrust_forward_b : Particles2D = $Particles2D_Thrust_ForwardB

onready var position_firepoint_a : Position2D = $Position2D_FirepointA
onready var position_firepoint_b : Position2D = $Position2D_FirepointB
onready var audio_fire : AudioStreamPlayer2D = $Audio_Fire
onready var audio_hit : AudioStreamPlayer2D = $Audio_Hit

onready var timer_reload : Timer = $Timer_Reload

var which_gun : bool = false

var health : int = 2
var ammo : int = 4
var reloading : bool = false

signal fighter_destroyed

func get_target_angle_diff() -> float:
	var target_angle : float = target.global_position.angle_to_point(self.global_position)
	var angle_diff : float = target_angle - self.global_rotation
	angle_diff = wrapf(angle_diff, -PI, PI)
	return angle_diff

func _physics_process(delta : float) -> void:
	particles_thrust_forward_a.emitting = false
	particles_thrust_forward_b.emitting = false
		
	if is_instance_valid(target):
		var angle_diff : float = get_target_angle_diff()
		if angle_diff < 0.0 and angular_velocity > -5.0:
			apply_torque_impulse(-ROTATION_SPEED * delta)
		elif angle_diff > 0.0 and angular_velocity < 5.0:
			apply_torque_impulse(ROTATION_SPEED * delta)
		var direction_to_target : Vector2 = self.global_position.direction_to(target.global_position)
		var velocity_to_target : float = (self.linear_velocity * direction_to_target).length()
		if linear_velocity.length() < MAX_SPEED:
			apply_impulse(Vector2.ZERO, Vector2.RIGHT.rotated(rotation) * MOVE_SPEED * delta)
			particles_thrust_forward_a.emitting = true
			particles_thrust_forward_b.emitting = true

func destroy() -> void:
	var explosion : Node2D = _Explosion.instance()
	explosion.global_position = global_position
	get_tree().current_scene.add_child(explosion)
	queue_free()
	emit_signal("fighter_destroyed")

func get_hit() -> void:
	audio_hit.play()
	health -= 1
	if health <= 0:
		destroy()

func fire() -> void:
	if reloading: return
	var bullet : Node2D = _EnemyBullet.instance()
	get_tree().current_scene.add_child(bullet)
	bullet.global_position = position_firepoint_a.global_position if which_gun else position_firepoint_b.global_position
	bullet.global_rotation = global_rotation
	bullet.velocity = Vector2.RIGHT.rotated(global_rotation) * SHOT_SPEED
	#apply_impulse(Vector2.ZERO, Vector2.LEFT.rotated(global_rotation) * RECOIL_FORCE)
	audio_fire.play()
	which_gun = !which_gun
	ammo -= 1
	if ammo <= 0:
		reloading = true
		timer_reload.start(1.0 + randf())

func _on_Timer_Fire_timeout() -> void:
	if not is_instance_valid(target):
		return
	var diff : float = get_target_angle_diff()
	var distance : float = self.global_position.distance_to(target.global_position)
	if abs(diff) < (PI / 8.0) and distance < 128.0 and distance > 32.0:
		fire()

func _on_Timer_Reload_timeout():
	ammo = rand_range(3, 6)
	reloading = false

func _on_FighterDrone_body_entered(body : PhysicsBody2D) -> void:
	if body is Asteroid:
		var impact_velocity : float = (body.linear_velocity - self.linear_velocity).length()
		if impact_velocity > 20.0:
			destroy()
		elif impact_velocity > 5.0:
			audio_hit.pitch_scale = 0.9 + (randf() * 0.2)
			audio_hit.volume_db = 0.0 if impact_velocity > 10.0 else -10.0 # janky hack, m8
			audio_hit.play()
