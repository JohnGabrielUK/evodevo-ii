extends RigidBody2D

const _Bullet : PackedScene = preload("res://objects/Bullet.tscn")
const _Explosion : PackedScene = preload("res://objects/particles/Explosion.tscn")

const SHOT_SPEED : float = 100.0
const RECOIL_FORCE : float = 2.5

onready var position_firepoint : Position2D = $Position2D_FirePoint
onready var audio_fire : AudioStreamPlayer2D = $Audio_Fire
onready var audio_hit : AudioStreamPlayer2D = $Audio_Hit

var connected : bool = false

func _on_ConnectorFemale_got_connection(owning_object : Node2D, snap_to : Position2D, linear_velocity : Vector2, angular_velocity : float) -> void:
	if connected:
		return
	owning_object.connect("fire", self, "fire")
	connected = true

func fire() -> void:
	var bullet : Node2D = _Bullet.instance()
	get_tree().current_scene.add_child(bullet)
	bullet.global_position = position_firepoint.global_position
	bullet.global_rotation = global_rotation
	bullet.velocity = Vector2.UP.rotated(global_rotation) * SHOT_SPEED
	apply_impulse(Vector2.ZERO, Vector2.DOWN.rotated(global_rotation) * RECOIL_FORCE)
	audio_fire.play()

func destroy() -> void:
	var explosion : Node2D = _Explosion.instance()
	explosion.global_position = global_position
	get_tree().current_scene.add_child(explosion)
	queue_free()

func _on_Gun_body_entered(body : PhysicsBody2D) -> void:
	if body is Asteroid:
		var impact_velocity : float = (body.linear_velocity - self.linear_velocity).length()
		if impact_velocity > 20.0:
			destroy()
		elif impact_velocity > 5.0:
			audio_hit.pitch_scale = 0.9 + (randf() * 0.2)
			audio_hit.volume_db = 0.0 if impact_velocity > 10.0 else -10.0 # janky hack, m8
			audio_hit.play()

func _on_ConnectorFemale_attracted(diff : Vector2) -> void:
	if not connected:
		apply_impulse(Vector2.ZERO, diff * (1.0/15.0))
