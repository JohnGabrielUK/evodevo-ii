extends RigidBody2D

class_name Mine

const _HugeExplosion : PackedScene = preload("res://objects/particles/HugeExplosion.tscn")

onready var sprite_lights : Sprite = $Sprite_Lights
onready var audio_beep : AudioStreamPlayer2D = $Audio_Beep
onready var area_warn : Area2D = $Area2D_Warn
onready var tween : Tween = $Tween

var time_since_last_beep : float = 0.0

signal destroyed

func destroy() -> void:
	var explosion : Node2D = _HugeExplosion.instance()
	explosion.global_position = global_position
	get_tree().current_scene.call_deferred("add_child", explosion)
	emit_signal("destroyed")
	queue_free()

func get_hit() -> void:
	destroy()

func beep() -> void:
	tween.interpolate_property(sprite_lights, "modulate", Color.white, Color.transparent, 0.2)
	tween.start()
	audio_beep.play()
	time_since_last_beep = 0.0

func _physics_process(delta : float) -> void:
	time_since_last_beep += delta
	for body in area_warn.get_overlapping_bodies():
		if body is Player:
			var beep_threshold : float = max(0.1, self.global_position.distance_to(body.global_position) - 38.0)
			audio_beep.pitch_scale = 1.5 if beep_threshold < 5.0 else 1.0
			beep_threshold /= 32.0
			beep_threshold += 0.1
			if time_since_last_beep > beep_threshold:
				beep()

func _on_Area2D_Trigger_body_entered(body : PhysicsBody2D) -> void:
	if body is Player:
		destroy()
