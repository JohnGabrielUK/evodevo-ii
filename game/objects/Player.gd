extends RigidBody2D

class_name Player

const _Explosion : PackedScene = preload("res://objects/particles/Explosion.tscn")

const MOVE_SPEED : float = 50.0
const ROTATION_SPEED : float = 100.0
const REGEN_RATE : float = 0.1

export (bool) var engines_disabled = false
export (Rect2) var area_bounds = Rect2(-8, -8, 1032, 1032)

onready var particles_thrust_forward : Particles2D = $Particles2D_Thrust_Forward
onready var particles_thrust_left : Particles2D = $Particles2D_Thrust_Left
onready var particles_thrust_right : Particles2D = $Particles2D_Thrust_Right
onready var audio_thrust : AudioStreamPlayer2D = $Audio_Thrust
onready var audio_hit : AudioStreamPlayer2D = $Audio_Hit
onready var audio_low_health : AudioStreamPlayer2D = $Audio_LowHealth

var regen_timeout : float = 0.0
var health : float = 1.0
var fuel_burned : float = 0.0
var shots_fired : int = 0
var hits_taken : int = 0

var time_since_last_shot : float = 0.0

var weapons_disabled : bool = false

signal fire
signal out_of_bounds
signal destroyed
signal asteroid_hit # For achievements
signal part_on_back # For achievements

func _physics_process(delta : float) -> void:
	time_since_last_shot += delta # Janky hack, m8
	if regen_timeout <= 0.0:
		health = clamp(health + (REGEN_RATE * delta), 0.0, 1.0)
	else:
		regen_timeout -= delta
	if not engines_disabled:
		var thrusting : bool = false
		if Input.is_action_pressed("move_up"):
			apply_impulse(Vector2.ZERO, Vector2.UP.rotated(rotation) * MOVE_SPEED * delta)
			particles_thrust_forward.emitting = true
			thrusting = true
			fuel_burned += delta
		else:
			particles_thrust_forward.emitting = false
		if Input.is_action_pressed("move_left"):
			apply_impulse(Vector2.ZERO, Vector2.LEFT.rotated(rotation) * MOVE_SPEED * delta)
			particles_thrust_left.emitting = true
			thrusting = true
			fuel_burned += delta
		else:
			particles_thrust_left.emitting = false
		if Input.is_action_pressed("move_right"):
			apply_impulse(Vector2.ZERO, Vector2.RIGHT.rotated(rotation) * MOVE_SPEED * delta)
			particles_thrust_right.emitting = true
			thrusting = true
			fuel_burned += delta
		else:
			particles_thrust_right.emitting = false
		if thrusting and not audio_thrust.playing:
			audio_thrust.play()
		elif not thrusting and audio_thrust.playing:
			audio_thrust.stop()
	else:
		particles_thrust_forward.emitting = false
		particles_thrust_left.emitting = false
		particles_thrust_right.emitting = false
		audio_thrust.stop()
	if Input.is_action_pressed("rotate_left"):
		apply_torque_impulse(-ROTATION_SPEED * delta)
	if Input.is_action_pressed("rotate_right"):
		apply_torque_impulse(ROTATION_SPEED * delta)
	if not weapons_disabled:
		if Input.is_action_just_pressed("fire") or (Settings.autofire and Input.is_action_pressed("fire") and time_since_last_shot > 0.2): # janky hack, m8
			shots_fired += 1
			time_since_last_shot = 0.0
			emit_signal("fire")
	# Have we gone out of bounds?
	if not area_bounds.has_point(global_position):
		emit_signal("out_of_bounds")

func destroy() -> void:
	var explosion : Node2D = _Explosion.instance()
	explosion.global_position = global_position
	get_tree().current_scene.add_child(explosion)
	queue_free()
	emit_signal("destroyed")

func get_fatal_collision_speed() -> float:
	if GameSession.current_difficulty == GameSession.DifficultyMode.NORMAL:
		return 30.0
	else:
		return 20.0

func get_hit() -> void:
	hits_taken += 1
	audio_hit.pitch_scale = 0.9 + (randf() * 0.2)
	audio_hit.volume_db = 3.0
	audio_hit.play()
	var old_health : float = health
	var damage : float = 0.05 + (randf() * 0.05)
	if GameSession.current_difficulty == GameSession.DifficultyMode.EXTREME:
		damage *= 2.0
	health -= damage
	if health <= 0.0:
		destroy()
	else:
		get_tree().call_group("player_camera", "shake_camera", self.global_position, 0.25)
		if old_health >= 0.5 and health < 0.5:
			audio_low_health.play()
	regen_timeout = 2.0

func _on_Player_body_entered(body) -> void:
	if body is Asteroid:
		var impact_velocity : float = (body.linear_velocity - self.linear_velocity).length()
		if impact_velocity > get_fatal_collision_speed():
			destroy()
		elif impact_velocity > 5.0:
			audio_hit.pitch_scale = 0.9 + (randf() * 0.2)
			audio_hit.volume_db = 0.0 if impact_velocity > 10.0 else -10.0 # janky hack, m8
			audio_hit.play()
		emit_signal("asteroid_hit")
	if body is TileMap:
		var impact_velocity : float = self.linear_velocity.length()
		if impact_velocity > get_fatal_collision_speed():
			destroy()
		elif impact_velocity > 5.0:
			audio_hit.pitch_scale = 0.9 + (randf() * 0.2)
			audio_hit.volume_db = 0.0 if impact_velocity > 10.0 else -10.0 # janky hack, m8
			audio_hit.play()

func _on_ConnectorMale_S_part_connected() -> void:
	emit_signal("part_on_back")
