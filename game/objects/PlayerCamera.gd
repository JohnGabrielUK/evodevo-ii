extends Camera2D

const SHAKE_COEFFICIENT : float = 16.0

export (NodePath) var path_target
onready var target : Node2D = get_node(path_target)

var shake_amount : float = 0.0

func _physics_process(delta : float) -> void:
	if is_instance_valid(target):
		global_position = target.global_position
	if shake_amount > 0.0:
		offset.x = (randf() - 0.5) * shake_amount * SHAKE_COEFFICIENT
		offset.y = (randf() - 0.5) * shake_amount * SHAKE_COEFFICIENT
		shake_amount -= delta
	else:
		offset = Vector2.ZERO

func shake_camera(at : Vector2, how_much : float) -> void:
	shake_amount = how_much / max(1.0, self.global_position.distance_to(at) / 10.0)
