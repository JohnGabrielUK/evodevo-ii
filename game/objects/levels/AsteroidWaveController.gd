extends Node

const _Asteroid1 : PackedScene = preload("res://objects/Asteroid1.tscn")
const _Asteroid2 : PackedScene = preload("res://objects/Asteroid2.tscn")
const _Asteroid3 : PackedScene = preload("res://objects/Asteroid3.tscn")

export (NodePath) var path_player
onready var player : Node2D = get_node(path_player)

export (NodePath) var path_achievement_popup
onready var achievement_popup : Control = get_node(path_achievement_popup)

onready var timer_next_phase : Timer = $Timer_NextPhase
onready var timer_spawn_asteroid : Timer = $Timer_SpawnAsteroid
onready var label_time_remaining : Label = $HUD/Label_TimeRemaining

enum LevelState {PRE_WAVE, WAVE}

var current_state : int  = LevelState.PRE_WAVE
var gun_on_back : bool = false

var asteroids_destroyed : int = 0

signal stage_complete
signal mission_failed

func _on_Timer_NextPhase_timeout() -> void:
	if current_state == LevelState.PRE_WAVE:
		current_state = LevelState.WAVE
		timer_next_phase.start(60)
		timer_spawn_asteroid.start(1)
	else:
	# Can't finish the stage if you already died
		if is_instance_valid(player):
			if gun_on_back:
				achievement_popup.get_achievement("wrong_socket", 2.5)
			GameProgress.maybe_update_stat(1, "most_asteroids_destroyed", asteroids_destroyed, false)
			emit_signal("stage_complete")

func _on_Timer_UpdateTimer_timeout() -> void:
	if current_state == LevelState.PRE_WAVE:
		label_time_remaining.text = "Asteroid wave begins in %.0f sec" % timer_next_phase.time_left
	else:
		label_time_remaining.text = "Survive for %.0f sec" % timer_next_phase.time_left

func get_asteroid_speed() -> float:
	if GameSession.current_difficulty == GameSession.DifficultyMode.EXTREME:
		return 30.0
	else:
		return 20.0

func spawn_asteroid() -> void:
	var asteroid : RigidBody2D
	var rand : float = randf()
	if rand > 0.66:
		asteroid = _Asteroid1.instance()
	elif rand > 0.33:
		asteroid = _Asteroid2.instance()
	else:
		asteroid = _Asteroid3.instance()
	get_tree().current_scene.add_child(asteroid)
	asteroid.global_position = player.global_position + Vector2(512 * randf(), 256 * randf()) + Vector2(0, -384)
	asteroid.rotation = PI * randf() * 2.0
	asteroid.apply_impulse(Vector2(-4.0, -4.0) + Vector2(8.0 * randf(), 8.0 * randf()), Vector2(-get_asteroid_speed(), get_asteroid_speed()) + Vector2(randf() * -10.0, randf() * 10.0))
	asteroid.connect("destroyed", self, "_on_Asteroid_destroyed")

func _on_Asteroid_destroyed() -> void:
	asteroids_destroyed += 1

func _on_Timer_SpawnAsteroid_timeout() -> void:
	if not is_instance_valid(player):
		return
	var number_of_asteroids : int = 2
	if GameSession.current_difficulty == GameSession.DifficultyMode.HARD:
		number_of_asteroids = 4
	elif GameSession.current_difficulty == GameSession.DifficultyMode.EXTREME:
		number_of_asteroids = 6
	for i in range(0, number_of_asteroids):
		spawn_asteroid()

# For this mission, going out of bounds means a game over
func _on_Player_out_of_bounds() -> void:
	emit_signal("mission_failed", "You went out of bounds.")

func _on_Player_part_on_back() -> void:
	gun_on_back = true

func _ready() -> void:
	if GameSession.current_difficulty == GameSession.DifficultyMode.EXTREME:
		timer_next_phase.start(10.0)
