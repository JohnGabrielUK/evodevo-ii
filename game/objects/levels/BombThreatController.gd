extends Node

const _Asteroid1 : PackedScene = preload("res://objects/Asteroid1.tscn")
const _Asteroid2 : PackedScene = preload("res://objects/Asteroid2.tscn")
const _Asteroid3 : PackedScene = preload("res://objects/Asteroid3.tscn")

const _FissionMailed : PackedScene = preload("res://objects/ui/FissionMailed.tscn")

export (NodePath) var path_player
onready var player : Node2D = get_node(path_player)
export (NodePath) var path_bomb
onready var bomb : Node2D = get_node(path_bomb)

export (NodePath) var path_achievement_popup
onready var achievement_popup : Control = get_node(path_achievement_popup)
export (NodePath) var path_oob_warning
onready var oob_warning : CanvasLayer = get_node(path_oob_warning)

onready var label_readout : Label = $HUD/Label_Readout
onready var label_time_remaining : Label = $HUD/Label_TimeRemaining
onready var anim_player : AnimationPlayer = $AnimationPlayer

var got_bomb : bool = false

signal stage_complete
signal mission_failed

func pick_position() -> Vector2:
	var done : bool = false
	var position : Vector2
	while not done:
		position = Vector2(1024.0 * randf(), 600.0 + (400.0 * randf()))
		if position.distance_to(player.global_position) > 64.0 and position.distance_to(bomb.global_position) > 64.0:
			done = true
	return position

func spawn_asteroids() -> void:
	for i in range(0, 100):
		var asteroid : RigidBody2D
		var rand : float = randf()
		if rand > 0.66:
			asteroid = _Asteroid1.instance()
		elif rand > 0.33:
			asteroid = _Asteroid2.instance()
		else:
			asteroid = _Asteroid3.instance()
		asteroid.global_position = pick_position()
		asteroid.rotation = randf() * PI * 2.0
		get_tree().current_scene.call_deferred("add_child", asteroid)

# For this mission, the player must leave with the pod in tow
func _on_Player_out_of_bounds() -> void:
	if got_bomb:
		if get_parent().clear_time < 120.0:
			achievement_popup.get_achievement("time_to_spare", 2.5)
		GameProgress.maybe_update_stat(3, "fastest_time", get_parent().clear_time, true)
		emit_signal("stage_complete")
	else:
		emit_signal("mission_failed", "You left without the bomb.")

func _on_Timer_UpdateReadout_timeout() -> void:
	if (not is_instance_valid(player)) or (not is_instance_valid(bomb)):
		return
	if bomb.time_left >= 0:
		label_time_remaining.text = "Bomb detonates in %.0f sec" % bomb.time_left
	else:
		label_time_remaining.text = ""
	label_time_remaining.modulate = Color("b8e3b8") if bomb.time_left > 30 else Color("d95763")
	if got_bomb: return
	label_readout.text = "Distance to bomb approx. %.1fm" % (player.global_position.distance_to(bomb.global_position) * 2.0)

func _on_Bomb_bomb_collected() -> void:
	got_bomb = true
	label_readout.text = "Bomb acquired. Leave the area."
	anim_player.play("got_bomb")
	oob_warning.hide()

func _on_Bomb_bomb_detonated() -> void:
	yield(get_tree().create_timer(0.5), "timeout")
	if is_instance_valid(player):
		emit_signal("mission_failed", "The bomb detonated.")

func _on_Timer_FissionMailed_timeout() -> void:
	randomize()
	if randf() > 0.90 and GameProgress.get_completion_percentage() >= 1.00:
		var fission_mailed : CanvasLayer = _FissionMailed.instance()
		get_tree().current_scene.add_child(fission_mailed)

func _ready() -> void:
	spawn_asteroids()
	label_time_remaining.text = "Bomb detonates in %.0f sec" % bomb.time_left
