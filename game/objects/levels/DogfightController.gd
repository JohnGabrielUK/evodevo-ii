extends Node

const _Asteroid1 : PackedScene = preload("res://objects/Asteroid1.tscn")
const _Asteroid2 : PackedScene = preload("res://objects/Asteroid2.tscn")
const _Asteroid3 : PackedScene = preload("res://objects/Asteroid3.tscn")
const _FighterDrone : PackedScene = preload("res://objects/FighterDrone.tscn")

const MAX_FIGHTERS : int = 3
const MAX_FIGHTERS_AT_END : int = 5

export (NodePath) var path_player
onready var player : Node2D = get_node(path_player)

export (NodePath) var path_achievement_popup
onready var achievement_popup : Control = get_node(path_achievement_popup)

onready var timer_stage_complete : Timer = $Timer_StageComplete
onready var label_time_remaining : Label = $HUD/Label_TimeRemaining
onready var label_shield_status : Label = $HUD/Label_ShieldStatus

var fighters_in_play : int = 0
var fighters_destroyed : int = 0

signal stage_complete
signal mission_failed

func pick_asteroid_position() -> Vector2:
	var done : bool = false
	var position : Vector2
	while not done:
		position = Vector2(1024.0 * randf(), 1024.0 * randf())
		if position.distance_to(player.global_position) > 64.0:
			done = true
	return position

func pick_fighter_position() -> Vector2:
	return player.global_position + (Vector2(256.0, 0.0).rotated(randf() * PI * 2.0))

func spawn_asteroids() -> void:
	for i in range(0, 200):
		var asteroid : RigidBody2D
		var rand : float = randf()
		if rand > 0.66:
			asteroid = _Asteroid1.instance()
		elif rand > 0.33:
			asteroid = _Asteroid2.instance()
		else:
			asteroid = _Asteroid3.instance()
		asteroid.global_position = pick_asteroid_position()
		asteroid.rotation = randf() * PI * 2.0
		get_tree().current_scene.call_deferred("add_child", asteroid)

func spawn_fighter() -> void:
	if fighters_in_play >= (MAX_FIGHTERS if timer_stage_complete.time_left > 30 else MAX_FIGHTERS_AT_END):
		return
	var fighter_drone : RigidBody2D = _FighterDrone.instance()
	fighter_drone.global_position = pick_fighter_position()
	fighter_drone.rotation = randf() * PI * 2.0
	fighter_drone.target = player
	get_tree().current_scene.call_deferred("add_child", fighter_drone)
	fighter_drone.connect("fighter_destroyed", self, "fighter_destroyed")
	fighters_in_play += 1

func _on_Player_out_of_bounds() -> void:
	emit_signal("mission_failed", "You left the cover of the asteroids.")

func fighter_destroyed() -> void:
	fighters_in_play -= 1
	fighters_destroyed += 1

func _on_Timer_SpawnFighter_timeout() -> void:
	if is_instance_valid(player):
		spawn_fighter()
	
func _on_Timer_StageComplete_timeout() -> void:
	# Can't finish the stage if you already died
	if not is_instance_valid(player):
		return
	if fighters_destroyed >= 25:
		achievement_popup.get_achievement("ultra_kill", 2.5)
	GameProgress.maybe_update_stat(4, "most_fighters_destroyed", fighters_destroyed, false)
	GameProgress.maybe_update_stat(4, "fewest_hits_taken", player.hits_taken, true)
	emit_signal("stage_complete")

func _on_Timer_UpdateTimer_timeout() -> void:
	label_time_remaining.text = "Survive for %.0f sec" % timer_stage_complete.time_left
	
func _on_Timer_UpdateShieldStatus_timeout():
	if is_instance_valid(player):
		label_shield_status.text = "Shield integrity: %.0f%%" % (player.health * 100.0)
		label_shield_status.modulate = Color("b8e3b8") if player.health > 0.5 else Color("d95763")

func _on_Player_destroyed() -> void:
	label_time_remaining.hide()
	label_shield_status.hide()

func _ready() -> void:
	spawn_asteroids()
	spawn_fighter()



