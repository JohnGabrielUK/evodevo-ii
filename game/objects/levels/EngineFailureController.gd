extends Node

const _Mine : PackedScene = preload("res://objects/Mine.tscn")

const AMMO_LIMIT : int = 30

export (NodePath) var path_player
onready var player : Node2D = get_node(path_player)

export (NodePath) var path_achievement_popup
onready var achievement_popup : Control = get_node(path_achievement_popup)

onready var label_ammo_remaining : Label = $HUD/Label_AmmoRemaining

var going_fast : bool = false
var mines_destroyed : int = 0
var shots_fired : int = 0

signal stage_complete

# For this mission, going out of bounds means the stage is clear
func _on_Player_out_of_bounds() -> void:
	if going_fast:
		achievement_popup.get_achievement("gotta_gun_fast", 2.5)
	GameProgress.maybe_update_stat(2, "fastest_time", get_parent().clear_time, true)
	GameProgress.maybe_update_stat(2, "most_mines_destroyed", mines_destroyed, false)
	GameProgress.maybe_update_stat(2, "fewest_shots_fired", shots_fired, true)
	emit_signal("stage_complete")

func pick_position() -> Vector2:
	var done : bool = false
	var position : Vector2
	while not done:
		position = Vector2(1024.0 * randf(), 1024.0 * randf())
		if position.distance_to(player.global_position) > 64.0:
			done = true
	return position

func get_number_of_mines() -> int:
	if GameSession.current_difficulty == GameSession.DifficultyMode.NORMAL:
		return 100
	else:
		return 200

func spawn_mines() -> void:
	for i in range(0, get_number_of_mines()):
		var mine : Node2D = _Mine.instance()
		mine.global_position = pick_position()
		get_tree().current_scene.call_deferred("add_child", mine)
		mine.connect("destroyed", self, "_on_Mine_destroyed")

func _on_Mine_destroyed() -> void:
	mines_destroyed += 1

func update_ammo_readout() -> void:
	if not is_instance_valid(player):
		return
	label_ammo_remaining.text = "Ammo remaining: " + str(AMMO_LIMIT - player.shots_fired)

func _on_Player_fire() -> void:
	if not is_instance_valid(player):
		return
	shots_fired += 1
	if GameSession.current_difficulty == GameSession.DifficultyMode.EXTREME:
		if player.shots_fired >= AMMO_LIMIT:
			player.weapons_disabled = true
			label_ammo_remaining.text = "Ammo supplies exhausted"
		else:
			update_ammo_readout()

func _physics_process(delta : float) -> void:
	if is_instance_valid(player):
		if player.linear_velocity.length() > 150.0:
			going_fast = true

func _ready() -> void:
	spawn_mines()
	if GameSession.current_difficulty == GameSession.DifficultyMode.EXTREME:
		label_ammo_remaining.show()
		update_ammo_readout()
