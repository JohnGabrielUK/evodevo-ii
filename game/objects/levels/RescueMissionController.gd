extends Node

const _Asteroid1 : PackedScene = preload("res://objects/Asteroid1.tscn")
const _Asteroid2 : PackedScene = preload("res://objects/Asteroid2.tscn")
const _Asteroid3 : PackedScene = preload("res://objects/Asteroid3.tscn")

const FUEL_LIMIT : float = 5.0

const TUTORIAL_MESSAGES : Array = [
	"Listen up, pilot. Let's make sure you remember how to fly that thing.",
	"Turn your ship with rotate_left and rotate_right, and thrust forwards with move_up.",
	"You can thrust to the side with move_left and move_right.",
	"Take a moment to get used to flying your ship. Those scientists can wait.",
	"If you see a red arrow flashing on screen, it means you're approaching the edge of the map, but you can't leave the area yet.",
	"For now, use the reading at the top to find your way to the escape pod.",
	"You've found it. Slowly dock with it using one of three ports on your ship (preferably the one on the rear).",
	"Good - now just fly off the edge of the map. The pod's mass will affect your ship's movement, so be careful."
]

export (NodePath) var path_player
onready var player : Node2D = get_node(path_player)
export (NodePath) var path_escape_pod
onready var escape_pod : Node2D = get_node(path_escape_pod)

export (NodePath) var path_achievement_popup
onready var achievement_popup : Control = get_node(path_achievement_popup)
export (NodePath) var path_oob_warning
onready var oob_warning : CanvasLayer = get_node(path_oob_warning)

onready var label_readout : Label = $HUD/Label_Readout
onready var label_fuel_remaining : Label = $HUD/Label_FuelRemaining
onready var label_tutorial : Label = $HUD/Label_Tutorial
onready var timer_next_tutorial_message : Timer = $Timer_NextTutorialMessage
onready var anim_player : AnimationPlayer = $AnimationPlayer
onready var anim_player_tutorial : AnimationPlayer = $AnimationPlayer_Tutorial

var got_pod : bool = false
var no_claims_bonus : bool = true

var tutorial_message : int = 0

signal stage_complete
signal mission_failed

func pick_position_for_player_or_pod() -> Vector2:
	return Vector2(200.0 + (randf() * 624.0), 200.0 + (randf() * 624.0))

func pick_positions_for_player_and_pod() -> void:
	var done : bool = false
	while not done:
		player.global_position = pick_position_for_player_or_pod()
		escape_pod.global_position = pick_position_for_player_or_pod()
		if player.global_position.distance_to(escape_pod.global_position) > 420.0:
			done = true

func get_number_of_asteroids() -> int:
	if GameSession.current_difficulty == GameSession.DifficultyMode.NORMAL:
		return 300
	else:
		return 500

func pick_position_for_asteroid() -> Vector2:
	var done : bool = false
	var position : Vector2
	while not done:
		position = Vector2(1024.0 * randf(), 1024.0 * randf())
		if position.distance_to(player.global_position) > 64.0 and position.distance_to(escape_pod.global_position) > 64.0:
			done = true
	return position

func spawn_asteroids() -> void:
	for i in range(0, get_number_of_asteroids()):
		var asteroid : RigidBody2D
		var rand : float = randf()
		if rand > 0.66:
			asteroid = _Asteroid1.instance()
		elif rand > 0.33:
			asteroid = _Asteroid2.instance()
		else:
			asteroid = _Asteroid3.instance()
		asteroid.global_position = pick_position_for_asteroid()
		asteroid.rotation = randf() * PI * 2.0
		get_tree().current_scene.call_deferred("add_child", asteroid)

# For this mission, the player must leave with the pod in tow
func _on_Player_out_of_bounds() -> void:
	if got_pod:
		if no_claims_bonus:
			achievement_popup.get_achievement("no_claims_bonus", 2.5)
		GameProgress.maybe_update_stat(0, "fastest_time", get_parent().clear_time, true)
		GameProgress.maybe_update_stat(0, "least_fuel_burned", player.fuel_burned, true)
		emit_signal("stage_complete")
	else:
		emit_signal("mission_failed", "You left without the escape pod.")

func update_fuel_readout() -> void:
	if player.fuel_burned < FUEL_LIMIT:
		label_fuel_remaining.text = "Fuel remaining: %.2f" % (FUEL_LIMIT - player.fuel_burned)
	else:
		player.engines_disabled = true
		label_fuel_remaining.text = "Fuel exhausted"
		if player.linear_velocity.length() < 5.0:
			emit_signal("mission_failed", "You ran out of fuel.")

func _on_Timer_UpdateReadout_timeout() -> void:
	if (not is_instance_valid(player)) or (not is_instance_valid(escape_pod)):
		return
	if got_pod: return
	label_readout.text = "Distance to pod approx. %.1fm" % (player.global_position.distance_to(escape_pod.global_position) * 2.0)

func _on_EscapePod_pod_rescued() -> void:
	got_pod = true
	label_readout.text = "Pod acquired. Leave the area."
	anim_player.play("got_pod")
	oob_warning.hide()
	if tutorial_message == 6:
		next_tutorial_message()

func _on_EscapePod_pod_destroyed() -> void:
	if not is_instance_valid(player):
		return
	yield(get_tree().create_timer(0.2), "timeout")
	emit_signal("mission_failed", "The escape pod was destroyed.")

func _on_PlayerOrPod_asteroid_hit() -> void:
	no_claims_bonus = false

func update_tutorial_message() -> void:
	var message_with_bindings : String = TUTORIAL_MESSAGES[tutorial_message]
	for binding in ["move_up", "move_left", "move_right", "rotate_left", "rotate_right"]:
		message_with_bindings = message_with_bindings.replace(binding, OS.get_scancode_string(Settings.get_key_binding(binding)))
	label_tutorial.text = message_with_bindings

func next_tutorial_message() -> void:
	tutorial_message += 1
	update_tutorial_message()
	if tutorial_message < 5:
		anim_player_tutorial.play("tutorial1")
		timer_next_tutorial_message.start(8.0)
	else:
		anim_player_tutorial.play("tutorial2")

func _physics_process(delta : float) -> void:
	if (not is_instance_valid(player)) or (not is_instance_valid(escape_pod)):
		return
	if GameSession.current_difficulty == GameSession.DifficultyMode.EXTREME:
		update_fuel_readout()
	if player.global_position.distance_to(escape_pod.global_position) < 80.0 and tutorial_message == 5:
		next_tutorial_message()

func _ready() -> void:
	pick_positions_for_player_and_pod()
	spawn_asteroids()
	player.rotation = randf() * PI * 2.0
	escape_pod.rotation = randf() * PI * 2.0
	if GameSession.current_difficulty == GameSession.DifficultyMode.EXTREME:
		label_fuel_remaining.show()
		update_fuel_readout()
	elif GameSession.current_difficulty == GameSession.DifficultyMode.NORMAL:
		yield(get_tree().create_timer(2.0), "timeout")
		update_tutorial_message()
		anim_player_tutorial.play("tutorial1")
		timer_next_tutorial_message.start()
