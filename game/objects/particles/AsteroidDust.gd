extends Sprite

const ANIM_SPEED : float = 10.0
const MOVE_SPEED : float = 10.0

var anim_index : float = 0.0

func _physics_process(delta : float) -> void:
	anim_index += (delta * ANIM_SPEED) / scale.x
	global_position += Vector2.UP.rotated(rotation) * MOVE_SPEED * delta
	if anim_index >= 6.0:
		queue_free()
	else:
		frame = anim_index
