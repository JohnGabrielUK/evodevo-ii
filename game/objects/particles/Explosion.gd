extends Node2D

const _Fireball : PackedScene = preload("res://objects/particles/Fireball.tscn")

onready var audio_explosion : AudioStreamPlayer2D = $Audio_Explosion
onready var particles : Particles2D = $Particles2D
onready var sprite_glow : Sprite = $Sprite_Glow
onready var tween : Tween = $Tween

func do_particles() -> void:
	for i in range(0, 3):
		var explosion : Sprite = _Fireball.instance()
		get_tree().current_scene.add_child(explosion)
		explosion.global_position = global_position
		explosion.rotation = randf() * PI * 2.0
		explosion.scale = Vector2.ONE * (1.0 + (randf() * 0.5))

func _ready() -> void:
	do_particles()
	audio_explosion.play()
	particles.emitting = true
	# Shake it, baby
	get_tree().call_group("player_camera", "shake_camera", self.global_position, 1.0)
	# Flash! Ah-aaaa!
	tween.interpolate_property(sprite_glow, "modulate", Color.white, Color.transparent, 0.05)
	tween.start()
