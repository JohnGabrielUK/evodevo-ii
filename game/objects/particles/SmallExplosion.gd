extends Node2D

const _Fireball : PackedScene = preload("res://objects/particles/Fireball.tscn")

onready var audio_explosion : AudioStreamPlayer2D = $Audio_Explosion
onready var particles : Particles2D = $Particles2D

func do_particles() -> void:
	for i in range(0, 3):
		var explosion : Sprite = _Fireball.instance()
		get_tree().current_scene.add_child(explosion)
		explosion.global_position = global_position
		explosion.rotation = randf() * PI * 2.0
		explosion.scale = Vector2.ONE * (0.5 + (randf() * 0.5))

func _ready() -> void:
	do_particles()
	audio_explosion.play()
	particles.emitting = true
