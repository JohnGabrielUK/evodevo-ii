extends Control

onready var label_a : Label = $LabelA
onready var label_b : Label = $LabelB
onready var audio_bonus : AudioStreamPlayer = $Audio_Bonus
onready var tween : Tween = $Tween

var achievements : Dictionary

func get_achievement(achievement_name : String, delay : float) -> void:
	if GameProgress.is_achievement_unlocked(achievement_name):
		return
	if delay > 0.0:
		yield(get_tree().create_timer(delay), "timeout")
	if tween.is_active():
		yield(tween, "tween_all_completed")
	var achievement_data : Dictionary = achievements[achievement_name]
	label_b.text = achievement_data["name"]
	tween.interpolate_property(self, "modulate", Color.transparent, Color.white, 0.75, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	tween.interpolate_property(self, "modulate", Color.white, Color.transparent, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN, 3.0)
	tween.start()
	audio_bonus.play()
	GameProgress.set_achievement_unlocked(achievement_name)
	GameProgress.save_game()

func _enter_tree() -> void:
	self.modulate = Color.transparent

func _ready() -> void:
	achievements = Utilities.load_data_from_json("res://data/achievements.json")
