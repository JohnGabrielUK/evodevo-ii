extends Control

const FONT_HEADING : Font = preload("res://fonts/ending.tres")
const FONT_PARAGRAPH : Font = preload("res://fonts/small.tres")

onready var active : bool = false

var achievements : Dictionary

signal close_achievements

func draw_text_with_shadow(font : Font, text : String, position : Vector2, colour : Color, centered : bool = false) -> void:
	var offset : Vector2 = Vector2.ZERO
	if centered:
		offset.x -= font.get_string_size(text).x / 2.0
	draw_string(font, position + offset + Vector2.ONE, text, Constants.COLOUR_SHADOW)
	draw_string(font, position + offset, text, colour)

func draw_achievement(position : Vector2, achievement_name : String) -> void:
	var colour_a : Color = Constants.COLOUR_BRIGHT
	var colour_b : Color = Constants.COLOUR_SHADE_A
	var icon_y : float = 32
	if GameProgress.is_achievement_unlocked(achievement_name):
		colour_a = Constants.COLOUR_VERY_BRIGHT
		colour_b = Constants.COLOUR_BRIGHT
		icon_y = 0
		draw_text_with_shadow(FONT_PARAGRAPH, "Achieved!", position + Vector2(770, 28), Constants.COLOUR_VERY_BRIGHT)
	else:
		draw_text_with_shadow(FONT_PARAGRAPH, "Unachieved", position + Vector2(750, 32), Constants.COLOUR_SHADE_A)
	var data : Dictionary = achievements[achievement_name]
	draw_text_with_shadow(FONT_HEADING, data["name"], position + Vector2(36, 14), colour_a)
	draw_text_with_shadow(FONT_PARAGRAPH, data["description"], position + Vector2(36, 32), colour_b)

func _draw() -> void:
	var cursor : Vector2 = Vector2(180, 220)
	for achievement_name in achievements.keys():
		draw_achievement(cursor, achievement_name)
		cursor.y += 52
	draw_text_with_shadow(FONT_PARAGRAPH, "Press any key to return", Vector2(40, 680), Constants.COLOUR_SHADE_A)
	draw_text_with_shadow(FONT_PARAGRAPH, "%d/5" % GameProgress.get_achievement_unlocked_count(), Vector2(1220, 680), Constants.COLOUR_BRIGHT, true)
	draw_text_with_shadow(FONT_PARAGRAPH, "Achievements achieved: ", Vector2(1070, 680), Constants.COLOUR_SHADE_A, true)

func _input(event : InputEvent) -> void:
	if not active: return
	if event.is_action_pressed("ui_cancel"):
		emit_signal("close_achievements")

func _ready() -> void:
	achievements = Utilities.load_data_from_json("res://data/achievements.json")
