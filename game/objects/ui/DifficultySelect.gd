extends Control

const FONT : Font = preload("res://fonts/menu.tres")
const DIFFICULTY_LABELS : Array = ["Normal", "Hard", "Extreme"]

onready var label_description : Label = $Label_Description
onready var audio_move : AudioStreamPlayer = $Audio_Move

var anim_index : float = 0.0
var selection_index : int = 0
var active : bool = false

func draw_string_w_offset(font : Font, position : Vector2, offset_ratio : float, text : String, modulate : Color):
	var label_width : float = font.get_string_size(text).x
	draw_string(font, Vector2(position.x - (label_width * offset_ratio), position.y), text, modulate)

func _draw() -> void:
	draw_string_w_offset(FONT, Vector2(0.0, 20.0), 0.0, "Difficulty: ", Constants.COLOUR_SHADE_B)
	draw_string_w_offset(FONT, Vector2(180.0, 20.0), 0.5, DIFFICULTY_LABELS[selection_index], Constants.COLOUR_SHADE_A)
	var wobble : float = sin(anim_index * 4.0) * 2.0
	draw_string_w_offset(FONT, Vector2(120.0 - wobble, 20.0), 0.0, "<", Constants.COLOUR_SHADE_A)
	draw_string_w_offset(FONT, Vector2(240.0 + wobble, 20.0), 1.0, ">", Constants.COLOUR_SHADE_A)

func change_selection(direction : int) -> void:
	selection_index = wrapi(selection_index + direction, 0, DIFFICULTY_LABELS.size())
	audio_move.play()
	label_description.text = Levels.get_level_difficulty_changes(GameSession.current_level, selection_index)

func make_active() -> void:
	active = true

func _process(delta : float) -> void:
	anim_index += delta
	if not active:
		update()
		return
	if Input.is_action_just_pressed("ui_left"):
		change_selection(-1)
	if Input.is_action_just_pressed("ui_right"):
		change_selection(1)
	update()

func _ready() -> void:
	selection_index = GameSession.current_difficulty
	label_description.text = Levels.get_level_difficulty_changes(GameSession.current_level, selection_index)
