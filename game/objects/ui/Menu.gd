extends Control

const FONT : Font = preload("res://fonts/menu.tres")
const SPRITE_CURSOR : StreamTexture = preload("res://sprites/ui/cursor.png")
const SPRITE_TICKBOX : StreamTexture = preload("res://sprites/ui/tickbox.png")
const SPRITE_SLIDER : StreamTexture = preload("res://sprites/ui/slider.png")

const SECRET_CODE : String = "rlrldduu"

onready var audio_back : AudioStreamPlayer = $Audio_Back
onready var audio_move : AudioStreamPlayer = $Audio_Move
onready var audio_select : AudioStreamPlayer = $Audio_Select
onready var audio_start : AudioStreamPlayer = $Audio_Start

signal button_pressed
signal variable_changed
signal menu_entered
signal back_from_root
signal secret_activated

onready var tween : Tween = $Tween

const VOLUME_INCR : float = 0.125
const ITEM_HEIGHT : float = 18.0

var items : Dictionary
var variables : Dictionary

var current_item : String
var current_child : int = 0
onready var active : bool = false
onready var rebinding : bool = false
var rebinding_action : String

var cursor_pos : float = 0.0

var secret_string : String = ""

func get_current_item() -> Dictionary:
	if items.has(current_item):
		return items[current_item]
	return {}

func get_current_children() -> Array:
	var item : Dictionary = get_current_item()
	if item.has("children"):
		return item["children"]
	return []

func get_label_for_item(slug : String) -> String:
	if items.has(slug):
		var item : Dictionary = items[slug]
		if item.has("label"):
			return item["label"]
	# Special cases
	if slug == "back":
		return "Back"
	return ""

func get_current_menu_child_slug(position : int) -> String:
	var item : Dictionary = items[current_item]
	if not item.has("children"): return ""
	return item["children"][position]

func get_child_type(slug : String) -> String:
	if not items.has(slug): return "null"
	var item : Dictionary = items[slug]
	if item.has("type"):
		return item["type"]
	return "null"

func is_child_button(slug : String) -> bool:
	return get_child_type(slug) == "button"

func is_child_menu(slug : String) -> bool:
	return get_child_type(slug) == "menu"

func is_child_variable(slug : String) -> bool:
	return get_child_type(slug) == "variable"

func is_child_key_binding(slug : String) -> bool:
	return get_child_type(slug) == "key_binding"

func get_select_size(options : Array) -> float:
	var max_option_size : float = 0.0
	for option in options:
		var option_size : Vector2 = FONT.get_string_size(option)
		max_option_size = max(option_size.x, max_option_size)
	return max_option_size + 24.0

func get_menu_item_size(menu_item : Dictionary) -> Vector2:
	var label : String = menu_item["label"]
	var label_size : Vector2 = FONT.get_string_size(label)
	if menu_item["type"] == "variable":
		var variable_name : String = menu_item["variable_name"]
		var variable : Dictionary = variables[variable_name]
		match variable["type"]:
			"tickbox":
				label_size.x += 16
			"volume":
				label_size.x += 24
			"select":
				label_size.x += get_select_size(variable["options"])
	elif menu_item["type"] == "key_binding":
		label_size.x += 64
	return label_size

func resize(instant : bool = false) -> void:
	var size : Vector2 = Vector2(0, 4)
	var children : Array = get_current_children()
	for i in range(0, children.size()):
		var child : String = children[i]
		var menu_item : Dictionary = items[child]
		var child_size : Vector2 = get_menu_item_size(menu_item)
		size = Vector2(max(size.x, child_size.x), size.y + 16)
	if instant:
		rect_min_size = size
		rect_size = size
		update()
	else:
		# Tween it
		tween.interpolate_property(self, "rect_size", rect_size, size, 0.25, Tween.TRANS_QUINT, Tween.EASE_OUT)
		tween.interpolate_property(self, "rect_min_size", rect_min_size, size, 0.25, Tween.TRANS_QUINT, Tween.EASE_OUT)
		tween.start()

func draw_text_with_shadow(text : String, position : Vector2, color : Color) -> void:
	draw_string(FONT, position + Vector2(1, 1), text, Constants.COLOUR_SHADOW)
	draw_string(FONT, position, text, color)

func draw_tickbox(variable : Dictionary, position : Vector2, is_current : bool) -> void:
	var value : bool = variable["value"]
	var tickbox_offset : float = (float(value) + (float(is_current) * 2)) * 12.0
	draw_texture_rect_region(SPRITE_TICKBOX, Rect2(position + Vector2(rect_size.x, 3), Vector2(12, 12)), Rect2(tickbox_offset, 0, 12, 12))

func draw_volume(variable : Dictionary, position : Vector2, is_current : bool) -> void:
	var empty_offset : float = ((float(is_current) * 2)) * 32.0
	var full_offset : float = (1 + (float(is_current) * 2)) * 32.0
	var fullness : float = variable["value"] * 32.0
	draw_texture_rect_region(SPRITE_SLIDER, Rect2(position + Vector2(rect_size.x - 12, 3), Vector2(32, 10)), Rect2(empty_offset, 0, 32, 10))
	draw_texture_rect_region(SPRITE_SLIDER, Rect2(position + Vector2(rect_size.x - 12, 3), Vector2(fullness, 10)), Rect2(full_offset, 0, fullness, 10))

func draw_select(variable : Dictionary, position : Vector2, is_current : bool) -> void:
	var color : Color = Constants.COLOUR_BRIGHT if is_current else Constants.COLOUR_SHADE_A
	var value : int = variable["value"]
	var options : Array = variable["options"]
	var label : String = options[value]
	var label_size : Vector2 = FONT.get_string_size(label)
	var max_label_size : float = get_select_size(options)
	draw_text_with_shadow(label, position + Vector2(rect_size.x - label_size.x + 4, 13), color)
	draw_text_with_shadow("<", position + Vector2(rect_size.x - max_label_size + 20, 13), color)
	draw_text_with_shadow(">", position + Vector2(rect_size.x + 8, 13), color)

func draw_key_binding(action_name : String, position : Vector2, is_current : bool) -> void:
	var color : Color = Constants.COLOUR_BRIGHT if is_current else Constants.COLOUR_SHADE_A
	if rebinding and rebinding_action == action_name:
		draw_text_with_shadow("...", position + Vector2(rect_size.x - FONT.get_string_size("...").x, 13), color)
	else:
		var binding_name : String = OS.get_scancode_string(Settings.get_key_binding(action_name))
		var binding_size : float = FONT.get_string_size(binding_name).x
		draw_text_with_shadow(binding_name, position + Vector2(rect_size.x - binding_size, 13), color)

func draw_variable(variable_name : String, position : Vector2, is_current : bool) -> void:
	var variable : Dictionary = variables[variable_name]
	match variable["type"]:
		"tickbox":
			draw_tickbox(variable, position, is_current)
		"volume":
			draw_volume(variable, position, is_current)
		"select":
			draw_select(variable, position, is_current)

func draw_menu_item(menu_item : Dictionary, position : Vector2, is_current : bool) -> void:
	var label : String = menu_item["label"]
	var color : Color = Constants.COLOUR_BRIGHT if is_current else Constants.COLOUR_SHADE_A
	draw_text_with_shadow(label, position + Vector2(4, 13), color)
	if menu_item["type"] == "variable":
		var variable_name : String = menu_item["variable_name"]
		draw_variable(variable_name, position, is_current)
	elif menu_item["type"] == "key_binding":
		var action_name : String = menu_item["action_name"]
		draw_key_binding(action_name, position, is_current)

func _draw() -> void:
	var offset : Vector2 = Vector2.ZERO
	var children : Array = get_current_children()
	for i in range(0, children.size()):
		var menu_item_name : String = children[i]
		var menu_item : Dictionary = items[menu_item_name]
		draw_menu_item(menu_item, offset, i == current_child)
		offset.y += ITEM_HEIGHT
	# Draw cursor
	draw_texture(SPRITE_CURSOR, Vector2(-10, 6 + cursor_pos))

func _process(delta : float) -> void:
	if tween.is_active():
		update()

func move_cursor() -> void:
	tween.interpolate_property(self, "cursor_pos", cursor_pos, current_child * ITEM_HEIGHT, 0.25, Tween.TRANS_QUINT, Tween.EASE_OUT)
	tween.start()

func toggle_tickbox(variable_slug : String) -> void:
	var value : bool = variables[variable_slug]["value"]
	value = !value
	variables[variable_slug]["value"] = value
	emit_signal("variable_changed", variable_slug)

func increase_volume(variable_slug : String) -> void:
	var value : float = variables[variable_slug]["value"]
	if value < 1.0:
		value += VOLUME_INCR
		variables[variable_slug]["value"] = value
		emit_signal("variable_changed", variable_slug)

func decrease_volume(variable_slug : String) -> void:
	var value : float = variables[variable_slug]["value"]
	if value > 0.0:
		value -= VOLUME_INCR
		variables[variable_slug]["value"] = value
		emit_signal("variable_changed", variable_slug)

func next_option(variable_slug : String) -> void:
	var variable : Dictionary = variables[variable_slug]
	var option_count : int = variable["options"].size()
	var value : int = variable["value"]
	value += 1
	if value >= option_count:
		value = 0
	variable["value"] = value
	emit_signal("variable_changed", variable_slug)

func previous_option(variable_slug : String) -> void:
	var variable : Dictionary = variables[variable_slug]
	var option_count : int = variable["options"].size()
	var value : int = variable["value"]
	value -= 1
	if value < 0:
		value = option_count - 1
	variable["value"] = value
	emit_signal("variable_changed", variable_slug)

func up() -> void:
	current_child -= 1
	if current_child < 0:
		current_child = get_current_children().size() - 1
	move_cursor()
	audio_move.play()

func down() -> void:
	current_child += 1
	if current_child >= get_current_children().size():
		current_child = 0
	move_cursor()
	audio_move.play()

func left_or_right(left : bool) -> void:
	var menu_item_name : String = get_current_children()[current_child]
	var menu_item : Dictionary = items[menu_item_name]
	if menu_item["type"] == "variable":
		var variable_name : String = menu_item["variable_name"]
		var variable : Dictionary = variables[variable_name]
		match variable["type"]:
			"volume":
				if left: decrease_volume(variable_name)
				else: increase_volume(variable_name)
			"select":
				if left: previous_option(variable_name)
				else: next_option(variable_name)
		audio_move.play()

func accept() -> void:
	var slug : String = get_current_menu_child_slug(current_child)
	if slug == "back":
		back()
	elif is_child_variable(slug):
		var variable_name : String = items[slug]["variable_name"]
		var variable_type : String = variables[variable_name]["type"]
		if variable_type == "tickbox":
			toggle_tickbox(variable_name)
			audio_select.play()
	elif is_child_button(slug):
		if slug != "play": # janky hack
			audio_select.play()
			pass
		emit_signal("button_pressed", slug)
	elif is_child_menu(slug):
		current_item = slug
		current_child = 0
		move_cursor()
		audio_select.play()
		emit_signal("menu_entered", current_item)
	elif is_child_key_binding(slug):
		rebinding = true
		rebinding_action = items[slug]["action_name"]
		audio_select.play()

func back() -> void:
	var item : Dictionary = get_current_item()
	if item.has("parent"):
		current_item = item["parent"]
		current_child = 0
		move_cursor()
		audio_back.play()
		emit_signal("menu_entered", current_item)
	else:
		emit_signal("back_from_root")

func set_active(active : bool) -> void:
	self.active = active

func do_secret(input : String) -> void:
	secret_string = input + secret_string
	if secret_string.length() > SECRET_CODE.length():
		secret_string = secret_string.substr(0, SECRET_CODE.length())
	if secret_string == SECRET_CODE:
		emit_signal("secret_activated")
		secret_string = ""

func _input(event : InputEvent) -> void:
	if not active: return
	get_tree().set_input_as_handled()
	if rebinding:
		if event is InputEventKey and event.pressed:
			Settings.set_key_binding(rebinding_action, event.scancode)
			rebinding = false
			audio_select.play()
			Settings.save_config()
	else:
		if Input.is_action_just_pressed("ui_up"):
			up()
			do_secret("u")
		elif Input.is_action_just_pressed("ui_down"):
			down()
			do_secret("d")
		elif Input.is_action_just_pressed("ui_left"):
			left_or_right(true)
			do_secret("l")
		elif Input.is_action_just_pressed("ui_right"):
			left_or_right(false)
			do_secret("r")
		elif Input.is_action_just_pressed("ui_accept"):
			accept()
		elif Input.is_action_just_pressed("ui_cancel"):
			back()
	resize()
	update()
