extends CanvasLayer

onready var label_failure_reason : Label = $Label_FailureReason
onready var anim_player : AnimationPlayer = $AnimationPlayer

func set_failure_reason(text : String) -> void:
	label_failure_reason.text = text

func _on_AnimationPlayer_animation_finished(anim_name : String) -> void:
	get_tree().change_scene("res://scenes/StageIntro.tscn")

func _ready() -> void:
	anim_player.play("stage_clear")
