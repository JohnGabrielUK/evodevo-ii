extends CanvasLayer

export (NodePath) var path_player
onready var player : Node2D = get_node(path_player)

export (Vector2) var area_bounds

const FUEL_LIMIT : float = 5.0

onready var chevron_top : TextureRect = $TextureRect_Chevrons_Top
onready var chevron_bottom : TextureRect = $TextureRect_Chevrons_Bottom
onready var chevron_left : TextureRect = $TextureRect_Chevrons_Left
onready var chevron_right : TextureRect = $TextureRect_Chevrons_Right

var anim_index : float = 0.0

var visible : bool = true

# Fair warning, this gets pretty janky
func _physics_process(delta : float) -> void:
	if (not is_instance_valid(player)) or (not visible):
		return
	anim_index += delta
	# Left
	if player.global_position.x > 200:
		chevron_left.visible = false
	elif player.global_position.x > 100:
		chevron_left.visible = fmod(anim_index, 0.5) > 0.25
	else:
		chevron_left.visible = fmod(anim_index, 0.25) > 0.125
	# Top
	if player.global_position.y > 200:
		chevron_top.visible = false
	elif player.global_position.y > 100:
		chevron_top.visible = fmod(anim_index, 0.5) > 0.25
	else:
		chevron_top.visible = fmod(anim_index, 0.25) > 0.125
	# Right
	if player.global_position.x < area_bounds.x - 200:
		chevron_right.visible = false
	elif player.global_position.x < area_bounds.x - 100:
		chevron_right.visible = fmod(anim_index, 0.5) > 0.25
	else:
		chevron_right.visible = fmod(anim_index, 0.25) > 0.125
	# Bottom
	if player.global_position.y < area_bounds.y - 200:
		chevron_bottom.visible = false
	elif player.global_position.y < area_bounds.y - 100:
		chevron_bottom.visible = fmod(anim_index, 0.5) > 0.25
	else:
		chevron_bottom.visible = fmod(anim_index, 0.25) > 0.125

func hide() -> void:
	visible = false
	chevron_top.hide()
	chevron_bottom.hide()
	chevron_left.hide()
	chevron_right.hide()
