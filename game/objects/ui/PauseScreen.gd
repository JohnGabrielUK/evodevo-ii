extends CanvasLayer

onready var control : Control = $Control
onready var menu : Control = $Control/Center/Menu
onready var blackout : ColorRect = $Control/Blackout
onready var audio_bgm : AudioStreamPlayer = $Audio_BGM
onready var tween : Tween = $Tween

func init_menu() -> void:
	# Set up the menu
	menu.items = {
		"pause_menu": {
			"type": "menu",
			"children": ["resume", "restart_level", "settings", "back_to_menu"]
		},
		"resume": {
			"type": "button",
			"label": "Resume"
		},
		"restart_level": {
			"type": "button",
			"label": "Restart Level"
		},
		"settings": {
			"label": "Settings",
			"type": "menu",
			"children": ["gameplay", "video", "audio", "controls", "back"],
			"parent": "pause_menu"
		},
		"gameplay": {
			"label": "Gameplay",
			"type": "menu",
			"children": ["autofire", "back"],
			"parent": "settings"
		},
		"autofire": {
			"label": "Autofire",
			"type": "variable",
			"variable_name": "autofire"
		},
		"video": {
			"label": "Video",
			"type": "menu",
			"children": ["fullscreen", "borderless_window", "show_cursor", "camera_shake", "back"],
			"parent": "settings"
		},
		"fullscreen": {
			"label": "Fullscreen",
			"type": "variable",
			"variable_name": "fullscreen"
		},
		"borderless_window": {
			"label": "Borderless Window",
			"type": "variable",
			"variable_name": "borderless_window"
		},
		"show_cursor": {
			"label": "Show Mouse Cursor",
			"type": "variable",
			"variable_name": "show_cursor"
		},
		"camera_shake": {
			"label": "Camera Shake",
			"type": "variable",
			"variable_name": "camera_shake"
		},
		"audio": {
			"label": "Audio",
			"type": "menu",
			"children": ["sfx_volume", "bgm_volume", "ui_volume", "back"],
			"parent": "settings"
		},
		"sfx_volume": {
			"label": "SFX",
			"type": "variable",
			"variable_name": "sfx_volume"
		},
		"bgm_volume": {
			"label": "Music",
			"type": "variable",
			"variable_name": "bgm_volume"
		},
		"ui_volume": {
			"label": "UI",
			"type": "variable",
			"variable_name": "ui_volume"
		},
		"controls": {
			"label": "Controls",
			"type": "menu",
			"children": ["key_binding_thrust_left", "key_binding_thrust_right", "key_binding_thrust_up", "key_binding_rotate_left", "key_binding_rotate_right", "key_binding_fire", "key_binding_pause", "back"],
			"parent": "settings"
		},
		"key_binding_thrust_left": {
			"label": "Thrust Left",
			"type": "key_binding",
			"action_name": "move_left"
		},
		"key_binding_thrust_right": {
			"label": "Thrust Right",
			"type": "key_binding",
			"action_name": "move_right"
		},
		"key_binding_thrust_up": {
			"label": "Thrust Forwards",
			"type": "key_binding",
			"action_name": "move_up"
		},
		"key_binding_rotate_left": {
			"label": "Spin Left",
			"type": "key_binding",
			"action_name": "rotate_left"
		},
		"key_binding_rotate_right": {
			"label": "Spin Right",
			"type": "key_binding",
			"action_name": "rotate_right"
		},
		"key_binding_fire": {
			"label": "Fire",
			"type": "key_binding",
			"action_name": "fire"
		},
		"key_binding_pause": {
			"label": "Pause",
			"type": "key_binding",
			"action_name": "pause"
		},
		"back_to_menu": {
			"type": "button",
			"label": "Back to Title Screen"
		},
		"back": {
			"type": "button",
			"label": "Back"
		}
	}
	menu.variables = {
		"autofire": {"type": "tickbox", "value": Settings.autofire},
		"fullscreen": {"type": "tickbox", "value": Settings.fullscreen},
		"borderless_window": {"type": "tickbox", "value": Settings.borderless_window},
		"show_cursor": {"type": "tickbox", "value": Settings.show_cursor},
		"camera_shake": {"type": "tickbox", "value": Settings.camera_shake},
		"sfx_volume": {"type": "volume", "value": Settings.sfx_volume},
		"bgm_volume": {"type": "volume", "value": Settings.bgm_volume},
		"ui_volume": {"type": "volume", "value": Settings.ui_volume}
	}
	menu.current_item = "pause_menu"
	menu.resize(true)
	menu.active = false

func _on_Menu_variable_changed(variable_name : String) -> void:
	Settings.autofire = menu.variables["autofire"]["value"]
	Settings.fullscreen = menu.variables["fullscreen"]["value"]
	Settings.borderless_window = menu.variables["borderless_window"]["value"]
	Settings.show_cursor = menu.variables["show_cursor"]["value"]
	Settings.sfx_volume = menu.variables["sfx_volume"]["value"]
	Settings.bgm_volume = menu.variables["bgm_volume"]["value"]
	Settings.ui_volume = menu.variables["ui_volume"]["value"]
	Settings.apply_config()
	Settings.save_config()

func _on_Menu_button_pressed(which : String) -> void:
	match which:
		"resume":
			hide_pause_menu()
		"restart_level":
			menu.active = false
			tween.interpolate_property(blackout, "modulate", Color.transparent, Color.white, 1.0)
			tween.interpolate_property(audio_bgm, "volume_db", null, -40.0, 1.0)
			tween.start()
			yield(tween, "tween_all_completed")
			get_tree().change_scene("res://scenes/StageIntro.tscn")
		"back_to_menu":
			menu.active = false
			tween.interpolate_property(blackout, "modulate", Color.transparent, Color.white, 1.0)
			tween.interpolate_property(audio_bgm, "volume_db", null, -40.0, 1.0)
			tween.start()
			yield(tween, "tween_all_completed")
			get_tree().change_scene("res://scenes/TitleScreen.tscn")

func _on_Menu_back_from_root() -> void:
	hide_pause_menu()

func show_pause_menu() -> void:
	get_tree().paused = true
	control.show()
	menu.active = true
	if audio_bgm.playing:
		audio_bgm.stream_paused = false
	else:
		audio_bgm.play()

func hide_pause_menu() -> void:
	get_tree().paused = false
	control.hide()
	menu.active = false
	audio_bgm.stream_paused = true

func _ready() -> void:
	init_menu()
