extends CanvasLayer

onready var anim_player : AnimationPlayer = $AnimationPlayer

func _on_AnimationPlayer_animation_finished(anim_name : String) -> void:
	GameSession.next_level()

func _ready() -> void:
	anim_player.play("stage_clear")
