extends Control

const FONT : Font = preload("res://fonts/menu.tres")

const CENTER : Vector2 = Vector2(96.0, 360.0)
const ITEM_OFFSET : Vector2 = Vector2(32.0, 64.0)

export (Vector2) var items_offset : Vector2 = Vector2.ZERO

onready var label_description : Label = $VBox/Label_Description
onready var label_prompt : Label = $Label_Prompt

onready var audio_back : AudioStreamPlayer = $Audio_Back
onready var audio_move : AudioStreamPlayer = $Audio_Move
onready var audio_select : AudioStreamPlayer = $Audio_Select
onready var audio_start : AudioStreamPlayer = $Audio_Start

onready var anim_player : AnimationPlayer = $AnimationPlayer

# For these variables, 1.0 is the distance between two items in the list
var scroll_target : float
var scroll_actual : float

var selection_index : int = 0

var active : bool = false

signal stage_selected
signal stage_select_closed

func draw_level(index : int, level : Dictionary, position : Vector2, selected : bool) -> void:
	var selected_scale : float = max(1.0, 1.5 - (abs(float(index) - scroll_actual) * 0.5)) # janky hack, m8!
	draw_set_transform(position + items_offset, 0.0, Vector2(selected_scale, selected_scale))
	var colour_a : Color = Constants.COLOUR_SHADE_A
	var colour_b : Color = Constants.COLOUR_SHADE_B
	if selected:
		colour_a = Constants.COLOUR_BRIGHT
		colour_b = Constants.COLOUR_SHADE_A
	draw_string(FONT, Vector2.ZERO, level["supertitle"], colour_a)
	draw_string(FONT, Vector2(0, 16), level["title"], colour_b)

func draw_level_locked(level : Dictionary, position : Vector2) -> void:
	draw_set_transform(items_offset, 0.0, Vector2.ONE)
	draw_string(FONT, position, level["supertitle"], Constants.COLOUR_SHADE_B)
	draw_string(FONT, position + Vector2(0, 16), "LOCKED", Constants.COLOUR_SHADE_B)

func draw_string_w_offset(font : Font, position : Vector2, offset_ratio : float, text : String, modulate : Color):
	var label_width : float = font.get_string_size(text).x
	draw_string(font, Vector2(position.x - (label_width * offset_ratio), position.y), text, modulate)

# Prepare for the jankiest of jank!
func draw_stats() -> void:
	draw_set_transform(Vector2.ZERO, 0.0, Vector2.ONE)
	var cursor : Vector2 = Vector2(label_description.rect_global_position.x + label_description.rect_size.x, 440.0)
	for stat_name in GameProgress.get_stat_names_for_level(selection_index):
		var stat_label : String = stat_name.replace("_", " ")
		var stat_value = GameProgress.get_stat(selection_index, stat_name)
		if stat_name == "fastest_time":
			stat_value = "%.02f sec" % stat_value
		elif stat_name == "least_fuel_burned":
			stat_value = "%.02f" % stat_value
		else:
			stat_value = str(stat_value)
		draw_string(FONT, cursor - Vector2(340.0, 0.0), stat_label, Constants.COLOUR_SHADE_A)
		draw_string_w_offset(FONT, cursor - Vector2(0.0, 0.0), 1.0, stat_value, Constants.COLOUR_BRIGHT)
		cursor.y += 20

func _draw() -> void:
	var cursor : Vector2 = CENTER - (scroll_actual * ITEM_OFFSET)
	for i in range(0, Levels.get_level_count()):
		var level : Dictionary = Levels.levels[i]
		if i <= GameProgress.levels_finished:
			draw_level(i, level, cursor, i == selection_index)
		else:
			draw_level_locked(level, cursor)
		cursor += ITEM_OFFSET
	draw_stats()

func move_selection(direction : int) -> void:
	selection_index = wrapi(selection_index + direction, 0, min(Levels.get_level_count(), GameProgress.levels_finished + 1))
	scroll_target = float(selection_index)
	audio_move.play()
	label_description.text = Levels.levels[selection_index]["description"].replace("\n", " ")

func appear() -> void:
	update_prompt()
	anim_player.play("in")
	yield(anim_player, "animation_finished")
	active = true

func reset_position() -> void:
	selection_index = 0
	scroll_target = 0.0
	scroll_actual = 0.0
	label_description.text = Levels.levels[selection_index]["description"].replace("\n", " ")

func update_prompt() -> void:
	label_prompt.text = "(%s) SELECT STAGE\n(%s) GO BACK" % [OS.get_scancode_string(Settings.get_key_binding("ui_accept")), OS.get_scancode_string(Settings.get_key_binding("ui_cancel"))]

func start_stage() -> void:
	emit_signal("stage_selected", selection_index)

func _process(delta : float) -> void:
	update()
	if not active:
		return
	if Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_left"):
		move_selection(-1)
	if Input.is_action_just_pressed("ui_down") or Input.is_action_just_pressed("ui_right"):
		move_selection(1)
	if Input.is_action_just_pressed("ui_accept"):
		start_stage()
	if Input.is_action_just_pressed("ui_cancel"):
		audio_back.play()
		active = false
		emit_signal("stage_select_closed")
	# Scroll
	scroll_actual = lerp(scroll_actual, scroll_target, delta * 5.0)

func _ready() -> void:
	label_description.text = Levels.levels[selection_index]["description"].replace("\n", " ")
