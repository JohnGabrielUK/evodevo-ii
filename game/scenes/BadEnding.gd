extends Control

const CHARS : String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?.,\"'-"

onready var label_message : Label = $Label_Message
onready var audio_bgm : AudioStreamPlayer = $Audio_BGM
onready var anim_player : AnimationPlayer = $AnimationPlayer

func corrupt_entire_message() -> void:
	var message : String = label_message.text
	for i in range(0, message.length()):
		var character : String = message[i]
		if character in CHARS:
			message[i] = CHARS[rand_range(0, CHARS.length())]
	label_message.text = message

func corrupt_random_letter() -> void:
	var message : String = label_message.text
	var index : int
	var found : bool = false
	while not found:
		index = rand_range(0, message.length())
		var character : String = message[index]
		if character in CHARS:
			message[index] = CHARS[rand_range(0, CHARS.length())]
			found = true
	label_message.text = message

func _on_Timer_Tick_timeout() -> void:
	for i in range(0, 10):
		corrupt_random_letter()

func _on_AnimationPlayer_animation_finished(anim_name : String) -> void:
	get_tree().change_scene("res://scenes/TitleScreen.tscn")

func _ready() -> void:
	get_tree().paused = false
	corrupt_entire_message()
	yield(get_tree().create_timer(2.0), "timeout")
	audio_bgm.play()
	anim_player.play("bad_ending")

func _enter_tree() -> void:
	$Blackout.modulate = Color.white
