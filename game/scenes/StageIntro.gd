extends Control

onready var label_stage_supertitle : Label = $Center/Grid/Label_StageNumber
onready var label_stage_title : Label = $Center/Grid/Label_StageName
onready var label_stage_description : Label = $Center/Grid/Label_StageDescription

onready var difficulty_select : Control = $Center/Grid/DifficultySelect

onready var starfield : Sprite = $CanvasLayer/Starfield
onready var fog1 : Sprite = $CanvasLayer/Fog1
onready var fog2 : Sprite = $CanvasLayer/Fog2

onready var audio_thud : AudioStreamPlayer = $Audio_Thud
onready var audio_start : AudioStreamPlayer = $Audio_Start

onready var anim_player : AnimationPlayer = $AnimationPlayer

var appearing : bool = true
var can_continue : bool = false

func _on_AnimationPlayer_animation_finished(anim_name : String) -> void:
	appearing = false
	can_continue = true

func _process(delta : float) -> void:
	starfield.region_rect.position.x += delta * 8.0
	fog1.region_rect.position.x += delta * 16.0
	fog2.region_rect.position.x += delta * 32.0
	if Input.is_action_just_pressed("ui_accept"):
		if appearing:
			anim_player.seek(3.0)
			appearing = false
			can_continue = true
		elif can_continue:
			can_continue = false
			difficulty_select.active = false
			GameSession.current_difficulty = difficulty_select.selection_index
			audio_start.play()
			anim_player.play("outro")
			yield(anim_player, "animation_finished")
			var scene_path : String = Levels.get_level_scene_path(GameSession.current_level)
			get_tree().change_scene(scene_path)

func _ready() -> void:
	label_stage_supertitle.text = Levels.get_level_supertitle(GameSession.current_level)
	label_stage_title.text = Levels.get_level_title(GameSession.current_level)
	label_stage_description.text = Levels.get_level_description(GameSession.current_level)
	appearing = true
	audio_thud.play()
	anim_player.play("intro")
	get_tree().paused = false
