extends Control

const CHARS : String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?.,\"'-"

onready var label_message : RichTextLabel = $Label_Message
onready var audio_bgm : AudioStreamPlayer = $Audio_BGM
onready var blackout : ColorRect = $Blackout
onready var anim_player : AnimationPlayer = $AnimationPlayer
onready var tween : Tween = $Tween

onready var timer_uncorrupt : Timer = $Timer_Uncorrupt

onready var true_message : String = label_message.text
var corrupt_message : String
var letters_to_uncorrupt : Array = []

var can_quit : bool = false

func make_corrupt_message() -> void:
	var message : String = label_message.bbcode_text
	for i in range(0, message.length()):
		var character : String = message[i]
		if character in CHARS:
			message[i] = CHARS[rand_range(0, CHARS.length())]
			letters_to_uncorrupt.append(i)
	corrupt_message = message
	seed(10)
	letters_to_uncorrupt.shuffle()

func corrupt_random_letter() -> void:
	var message : String = corrupt_message
	var index : int
	var found : bool = false
	while not found:
		index = rand_range(0, message.length())
		var character : String = message[index]
		if character in CHARS and index in letters_to_uncorrupt:
			message[index] = CHARS[rand_range(0, CHARS.length())]
			found = true
	corrupt_message = message

func uncorrupt_letter() -> void:
	var index : int = letters_to_uncorrupt.pop_back()
	label_message.text[index] = true_message[index]

# Janky hack, m8
func build_colourful_message() -> String:
	var result : String = ""
	for i in range(0, true_message.length()):
		if i in letters_to_uncorrupt:
			result += "[color=#ac3232]" + corrupt_message[i] + "[/color]"
		else:
			result += "[color=#c97878]" + true_message[i] + "[/color]"
	return result

func _on_Timer_Tick_timeout() -> void:
	if letters_to_uncorrupt.empty(): return
	for i in range(0, 10):
		corrupt_random_letter()
	label_message.bbcode_text = build_colourful_message()
	
func _on_Timer_Uncorrupt_timeout() -> void:
	if not letters_to_uncorrupt.empty():
		uncorrupt_letter()
		label_message.bbcode_text = build_colourful_message()
	else:
		timer_uncorrupt.stop()
		can_quit = true
		return

func _input(event : InputEvent):
	if not can_quit: return
	if event is InputEventKey and event.pressed:
		can_quit = false
		tween.interpolate_property(blackout, "modulate", Color.transparent, Color.white, 4.0)
		tween.interpolate_property(audio_bgm, "volume_db", null, -40.0, 6.0)
		tween.start()
		yield(tween, "tween_all_completed")
		get_tree().change_scene("res://scenes/TitleScreen.tscn")

func _ready() -> void:
	get_tree().paused = false
	make_corrupt_message()
	yield(get_tree().create_timer(2.0), "timeout")
	audio_bgm.play()
	anim_player.play("true_ending")

func _enter_tree() -> void:
	$Blackout.modulate = Color.white
