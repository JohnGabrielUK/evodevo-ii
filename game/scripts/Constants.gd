extends Node

# UI colours
const COLOUR_VERY_BRIGHT : Color = Color("def2de")
const COLOUR_BRIGHT : Color = Color("b8e3b8")
const COLOUR_SHADE_A : Color = Color("79b379")
const COLOUR_SHADE_B : Color = Color("2c5d2c")
const COLOUR_SHADOW : Color = Color("082108")
