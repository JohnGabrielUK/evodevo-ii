extends Node2D

const _StageClear : PackedScene = preload("res://objects/ui/StageClear.tscn")
const _MissionFailed : PackedScene = preload("res://objects/ui/MissionFailed.tscn")

onready var blackout : ColorRect = $Blackout/ColorRect
onready var pause_screen : CanvasLayer = $PauseScreen
onready var audio_bgm : AudioStreamPlayer = $Audio_BGM
onready var tween : Tween = $Tween

enum GameState {NORMAL, GAME_OVER}

var game_state : int = GameState.NORMAL

var clear_time : float = 0.0

# Since each stage's win conditions are unique, we wait for a controller to tell us we've won
func stage_clear() -> void:
	var stage_clear : CanvasLayer = _StageClear.instance()
	add_child(stage_clear)
	get_tree().paused = true

func _mission_failed(reason : String) -> void:
	var mission_failed : CanvasLayer = _MissionFailed.instance()
	add_child(mission_failed)
	mission_failed.set_failure_reason(reason)
	get_tree().paused = true

func _on_Player_destroyed() -> void:
	audio_bgm.stop()
	game_state = GameState.GAME_OVER
	tween.interpolate_property(blackout, "modulate", Color.transparent, Color.white, 2.0, Tween.TRANS_LINEAR, Tween.EASE_OUT, 1.0)
	tween.start()
	yield(tween, "tween_all_completed")
	yield(get_tree().create_timer(1.0), "timeout")
	get_tree().change_scene("res://scenes/StageIntro.tscn")

func _physics_process(delta):
	clear_time += delta # Yes, this is the jankiest possible way of doing it. Don't care.

func _input(input : InputEvent) -> void:
	if input.is_action_pressed("pause") and game_state == GameState.NORMAL:
		pause_screen.show_pause_menu()

func _enter_tree() -> void:
	$Blackout/ColorRect.modulate = Color.white

func _ready() -> void:
	tween.interpolate_property(blackout, "modulate", Color.white, Color.transparent, 1.0)
	tween.start()
	yield(get_tree().create_timer(0.01), "timeout") # Sync up the audio
	audio_bgm.play()
