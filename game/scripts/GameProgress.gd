extends Node

const SAVE_VERSION : int = 3
const SAVE_PATH : String = "user://save.json"
const BACKUP_PATH : String = "user://save.bak"

var levels_finished : int
var level_difficulties_finished : Array
var stats : Array
var achievements_unlocked : Array
var new_game : bool

func get_completion_percentage() -> float:
	return float(get_achievement_unlocked_count()) / 5.0 # ew, hard-coded stuff!

func is_achievement_unlocked(achievement_name : String) -> bool:
	return achievements_unlocked.has(achievement_name)

func set_achievement_unlocked(achievement_name : String) -> void:
	if not achievements_unlocked.has(achievement_name):
		achievements_unlocked.append(achievement_name)

func get_achievement_unlocked_count() -> int:
	return achievements_unlocked.size()

func get_stat_names_for_level(level_id : int) -> Array:
	return stats[level_id]

func has_stat(level_id : int, stat_name : String) -> bool:
	return stats[level_id].has(stat_name)

func get_stat(level_id : int, stat_name : String):
	return stats[level_id][stat_name]

func maybe_update_stat(level_id : int, stat_name : String, stat_value, lower_is_better : bool) -> void:
	# If this is the first value for this stat, just set it
	if not has_stat(level_id, stat_name):
		stats[level_id][stat_name] = stat_value
	# Otherwise, see if this supercedes the existing stat
	var better_stat : bool = false
	if lower_is_better:
		if stat_value < get_stat(level_id, stat_name):
			better_stat = true
	else:
		if stat_value > get_stat(level_id, stat_name):
			better_stat = true
	if better_stat:
		stats[level_id][stat_name] = stat_value
	save_game()

# This is utter jank but I'm just including it for the sake of future-proofing;
# if I ever do a v1.3 I'll want to track which difficulties a player has completed,
# and I can't be arsed to do it properly right now.
func set_level_difficulty_finished(level_id : int, difficulty : int) -> void:
	var difficulty_string : String = str(level_id) + "-" + str(difficulty)
	if not level_difficulties_finished.has(difficulty_string):
		level_difficulties_finished.append(difficulty_string)

func save_exists() -> bool:
	var file : File = File.new()
	return file.file_exists(SAVE_PATH)

func save_game() -> void:
	var json : Dictionary = {
		"save_version": SAVE_VERSION,
		"levels_finished": levels_finished,
		"level_difficulties_finished": level_difficulties_finished,
		"stats": stats,
		"achievements_unlocked": achievements_unlocked,
		"new_game": new_game
	}
	var content : String = to_json(json)
	var file : File = File.new()
	file.open(SAVE_PATH, File.WRITE)
	file.store_string(content)
	file.close()

func load_game() -> void:
	var json : Dictionary = Utilities.load_data_from_json(SAVE_PATH)
	# A bit of bodging to bring old saved games up to spec
	if json.has("save_version"):
		# This save file is from an early build of v1.2 given to playtesters
		if json["save_version"] == 1:
			json["stats"] = [{}, {}, {}, {}, {}]
			json["level_difficulties_finished"] = []
		# This save file is from an later build of v1.2 given to playtesters
		elif json["save_version"] == 2:
			json["level_difficulties_finished"] = []
		# Snake! You've created a time paradox!
		elif json["save_version"] > SAVE_VERSION:
			OS.alert("Your saved game appears to be from a newer version of Evodevo II.\nThe game will attempt to start anyway, but you may experience crashes and/or corruption of the save file.\nJust to be on the safe side, we'll make a backup copy of your saved game.", "Save file version mismatch")
			var d : Directory = Directory.new()
			d.copy(SAVE_PATH, BACKUP_PATH)
	else:
		# This save file is from v1.1
		json["levels_finished"] = 0
		json["level_difficulties_finished"] = []
		json["stats"] = [{}, {}, {}, {}, {}]
		json["new_game"] = true
		save_game()
	levels_finished = json["levels_finished"]
	level_difficulties_finished = json["level_difficulties_finished"]
	stats = json["stats"]
	achievements_unlocked = json["achievements_unlocked"]
	new_game = json["new_game"]

func new_game() -> void:
	achievements_unlocked = []
	levels_finished = 0
	level_difficulties_finished = []
	stats = [{}, {}, {}, {}, {}]
	new_game = true
	save_game()

func _ready() -> void:
	if save_exists():
		load_game()
	else:
		new_game()
