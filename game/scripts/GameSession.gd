extends Node

enum DifficultyMode {NORMAL, HARD, EXTREME}

var current_level : int
var current_difficulty : int = 0
var joke_level : bool = false

func new_game() -> void:
	current_difficulty = GameSession.DifficultyMode.NORMAL
	current_level = 0

func next_level() -> void:
	# If we were just playing a joke level, go back to the title screen
	if joke_level:
		get_tree().change_scene("res://scenes/TitleScreen.tscn")
		joke_level = false
		return
	GameProgress.set_level_difficulty_finished(GameSession.current_level, GameSession.current_difficulty)
	GameProgress.levels_finished = max(GameProgress.levels_finished, current_level + 1)
	GameProgress.new_game = false
	GameProgress.save_game()
	# Go to the next level/ending
	current_level += 1
	if current_level < Levels.get_level_count():
		get_tree().change_scene("res://scenes/StageIntro.tscn")
	else:
		if GameProgress.get_completion_percentage() >= 1.0:
			get_tree().change_scene("res://scenes/TrueEnding.tscn")
		else:
			get_tree().change_scene("res://scenes/BadEnding.tscn")
