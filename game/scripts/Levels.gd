extends Node

var levels : Array = Utilities.load_data_from_json("res://data/levels.json")["levels"]

func get_level_supertitle(level_id : int) -> String:
	return levels[level_id]["supertitle"]

func get_level_title(level_id : int) -> String:
	return levels[level_id]["title"]

func get_level_description(level_id : int) -> String:
	return levels[level_id]["description"]

func get_level_objectives(level_id : int) -> String:
	return levels[level_id]["objectives"]

func get_level_difficulty_changes(level_id : int, difficulty : int) -> String:
	return levels[level_id]["difficulty_changes"][difficulty]

func get_level_scene_path(level_id : int) -> String:
	return levels[level_id]["scene_path"]

# Returns the number of non-joke levels
func get_level_count() -> int:
	var result : int = 0
	for level in levels:
		if not "joke" in level["flags"]:
			result += 1
	return result
